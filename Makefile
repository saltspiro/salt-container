.PHONY: docker
docker: saltstack.gpg
	docker build .

saltstack.gpg:
	curl https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub | gpg --import -
	gpg --export 0E08A149DE57BFBE > $@
